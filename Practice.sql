--1
SELECT 
	categories.id ,
	categories.name, 
    count(items.category_id) as count
FROM 
	categories
    INNER JOIN items
    ON categories.id = items.category_id
WHERE 
    categories.id = items.category_id
GROUP BY
	categories.id;
--2
SELECT 
	 categories.id ,
	 categories.name, 
     sum(items.amount) as sum_amount
FROM 
	categories
	INNER JOIN items
	ON categories.id = items.category_id
WHERE 
	categories.id = items.category_id
GROUP BY
	categories.id;
--3
SELECT 
	 categories.id ,
	 categories.name
FROM 
	categories
	INNER JOIN items
	ON categories.id = items.category_id
WHERE 
    items.amount > 40
GROUP BY
	categories.id;
--4
DELETE categories
FROM categories
    LEFT JOIN items
    ON categories.id = items.category_id
WHERE items.category_id IS NULL;