<?php

function checkValidString($chuoi) {
    $value1 = "book";
    $value2 = "restaurant";
    //kiểm tra chuỗi có tồn tại không 
    $case1 = substr_count($chuoi, $value1);
    $case2 = substr_count($chuoi, $value2);
    //xét điều kiện
    return $case1 >= 1 ? $case2 === 0 : $case2 >= 1;
};

$file1 = @fopen('file1.txt', 'r');
if (!$file1)
    echo "Open file1.txt failed<br>";
else {
    $chuoi1 = fread($file1, filesize('file1.txt'));
}

$file2 = @fopen('file2.txt', 'r');
if (!$file2)
    echo "Open file2.txt failed<br>";
else {
    $chuoi2 = fread($file2, filesize('file2.txt'));
}

if (checkValidString($chuoi1)) {
    $n = substr_count($chuoi1, ".");
    echo "Chuỗi  hợp lệ. chuỗi bao gồm $n câu.<br>";
}   else {
    echo "Chuỗi  không hợp lệ.<br>";
}

if (checkValidString($chuoi2)) {
    $n = substr_count($chuoi2, ".");
    echo "Chuỗi  hợp lệ. chuỗi bao gồm $n câu.";
}   else {
    echo "Chuỗi  không hợp lệ.";
}
?>