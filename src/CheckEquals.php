<?php

function checkEquals($a, $b) 
{
    $N = count($a);
    $M = count($b);
    if ($N != $M)
        return false;

    for ( $i = 0; $i < $N; $i++) {
        for( $j = 0; $j < $M; $j++) {
            if ($a[$i] == $b[$j]) {
                array_splice($b, $j, 1);
            }
        }
    }
    return count($b) == 0;
}

$a = [2, 3, 5, 2, 5, 2];
$b = [5, 3, 2, 2, 5, 2];

echo var_dump(checkEquals($a, $b)); 
?>