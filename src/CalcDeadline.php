<?php

function calcDeadline($manday, $calendar) {
    $a = count($calendar);
    // $str = array_reduce($calendar, function ($carry, $value){
    //     if ($carry ){
    //         return $carry . $value;
    //     }
    //     return $value;
    // });
    // echo $str;
    $str = implode($calendar);
    $b = substr_count($str , 1);
    $result = $b + $manday ;
    if($result <= $a) {
        return $result;
    } else {
        return false;
    }
}
$calendar = [
    false, // tương đương với ngày 0
    false, // tương đương với ngày 1
    true,  // tương đương với ngày 2
    true,  // tương đương với ngày 3
    false, // tương đương với ngày 4
    false, // tương đương với ngày 5
    true,  // tương đương với ngày 6
    false, // tương đương với ngày 7
];

var_dump(calcDeadline(9, $calendar));
var_dump(calcDeadline(7, $calendar));
var_dump(calcDeadline(5, $calendar));