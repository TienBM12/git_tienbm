<?php

interface TreeInterface
{
    public function init($array);
    public function getNode($id);
    public function getNodes($id);
    public function getParent($id);
    public function toString(); 
}

class Node
{
    public $id;
    public $parentId;
    public $name;

    function __construct($id, $parentId, $name) {
        $this->id = $id;
        $this->parentId = $parentId;
        $this->name = $name;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getParentId() {
        return $this->parentId;
    }

    public function setParentID($parentId) {
        $this->parentId = $parentId;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
}

class Tree implements TreeInterface 
{
    public $NodeList = [];
    public $data;

    public function init($array) {
        foreach($array as $node) {
            if(count($this->getNode($node['id'])) === 0) {
                array_push($this->NodeList, new Node($node['id'], $node['parent_id'], $node['name']));
            }
        }
    }

    public function buildTree($array, $parentId = 0) {
        $branch = [];
        foreach($array as $node) {
            if($node['parent_id'] == $parentId) {
                $children = $this->buildTree($array, $node['id']);
                if($children) {
                    $node['children'] = $children;
                }
                $branch[$node['id']] = $node;
                unset($array[$node['id']]);
            }
        }
        return $branch;
    }

    public function getNode($id) {
       foreach($this->NodeList as $node) {
           if($node->getId() === $id) {
               return [
                   "id" => $node->getId(),
                   "parent_id" => $node->getParentId(),
                   "name" => $node->getName(),
               ];
           }
       }
       return [];
    }

    public function getNodes($id){
        $Nodes = [];
        foreach($this->NodeList as $node) {
            if($node->getId() === $id) {
                array_push($Nodes , [
                    "id" => $node->getId(),
                    "parent_id" => $node->getParentId(),
                    "name" => $node->getName(),
                ]);
            }
        }
        return $Nodes;
    }

    public function getParent($id){
        foreach($this->NodeList as $node) {
            if($node->getId() === $id) {
                return $this->getNode($node->getParentId());
            }
        }
        return [];
    }

    public function getChildNodeToString($id) {
        $str = "";

        foreach($this->data as $key => $value) {
            if($value->getParentId() === $id && $value->getId() === $id) {
                $str .= "<li>".$value->getName()."</li>";
                unset($this->data[$key]);
            }
            if($value->getParentId() === $id && $value->getId() !== $id) {
                $str .= "<li>".$value->getName()."<ul>".$this->getChildNodeToString($value->getId())."</ul></li>";
                unset($this->data[$key]);
            }
        }
        return $str;
    }

    public function nodesToString() {
        if(!isset($this->data)) {
            $this->data = $this->NodeList;
        }

        $str="";

        foreach($this->data as $key => $node) {
            if(count($this->getParent($node->getId())) > 0) {
                continue;
            }
            $str .= "<li>".$node->getName()."<ul>";
            $str .= $this->getChildNodeToString($node->getId());
            $str .= "</ul></li>";
        }
        return $str;
    }

    public function toString() {
        return "<ul>".$this->nodesToString()."</ul>";
    }
}

$directNodes = [
    ['id' => 1, 'name' => "Node A",  'parent_id' => 0],
    ['id' => 2, 'name' => "Node B",  'parent_id' => 0],
    ['id' => 3, 'name' => "Node A1", 'parent_id' => 1],
    ['id' => 4, 'name' => "Node A2", 'parent_id' => 1],
    ['id' => 5, 'name' => "Node A1.1",  'parent_id' => 3],
    ['id' => 6, 'name' => "Node B1",  'parent_id' => 2],
    ['id' => 7, 'name' => "Node B2", 'parent_id' => 2],
    ['id' => 8, 'name' => "Node A2.1", 'parent_id' => 4],
];

$tree = new Tree();
$tree->init($directNodes);
echo $tree->toString();
