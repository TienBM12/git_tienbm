<?php

function calcMinPrice($price, $stepPrices, $rivalryPrice) {
    $M = count($stepPrices);
    while($price <= $rivalryPrice) {
        // foreach($stepPrices as $key => $value) {
        // }
        switch ($price) {
            case $price >= 100 && $price <300 :
                $price = $price + 40;
            break;
            case $price >= 300 && $price <500 :
                $price = $price + 60;
            break;
            case $price >= 500 && $price <1000 :
                $price = $price + 80;
            break;
            case $price >= 1000 && $price <1500 :
                $price = $price + 120;
            break;
            case $price >= 1500  :
                $price = $price + 150;
            break;
        }
    }
    return $price;
}
$stepPrices = [
    100  => 40,
    300  => 60,
    500  => 80,
    1000 => 120,
    1500 => 150,
];

$price = 210;
$rivalryPrice = 1200;
echo calcMinPrice($price, $stepPrices, $rivalryPrice);
?>