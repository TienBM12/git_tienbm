<?php 

abstract class Country 
{
    protected $slogan;
    public abstract function sayHello();

    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;
    } 
}

interface Boss
{
    public function checkValidSlogan();
}

trait Active 
{
    public function defindYourSelf() {
        return get_class($this);
    }
}

class EnglandCountry extends Country implements Boss
{
    use Active;

    public function checkValidSlogan(){
        $value1 = "england";
        $value2 = "english";
    
        $chuoi = strtolower($this->slogan);
        $case1 = substr_count($chuoi, $value1);
        $case2 = substr_count($chuoi, $value2);
        
        // return $case1 >= 1 ? true : ( $case2 >= 1 ? true : false ) ;
        return $case1 || $case2; 
	}

    public function sayHello()
    {
        echo "Hello";
    }
}

class VietnamCountry extends Country implements Boss
{
    use Active;

    public function checkValidSlogan(){
        $value1 = "vietnam";
        $value2 = "hust";

        $chuoi = strtolower($this->slogan);
        $case1 = substr_count($chuoi, $value1);
        $case2 = substr_count($chuoi, $value2);

        // return $case1 >= 1 ?  $case2 >= 1 : $case2 === 0 ;
        return $case1 && $case2;
	}

    public function sayHello()
    {
        echo "Xin chào";
    }
}
$englandCountry = new EnglandCountry();
$vietnamCountry = new VietnamCountry();

$englandCountry->setSlogan('England is a country that is part of the United Kingdom. It shares land borders with Wales to the west and Scotland to the north. The Irish Sea lies west of England and the Celtic Sea to the southwest.');
$vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. With an estimated 94.6 million inhabitants as of 2016, it is the 15th most populous country in the world.');

$englandCountry->sayHello(); // Hello
echo "<br>";
$vietnamCountry->sayHello(); // Xin chao
echo "<br>";

var_dump($englandCountry->checkValidSlogan()); // true
echo "<br>";
var_dump($vietnamCountry->checkValidSlogan()); // false
echo "<br>";
echo 'I am ' . $englandCountry->defindYourSelf(); 
echo "<br>";
echo 'I am ' . $vietnamCountry->defindYourSelf(); 

?>