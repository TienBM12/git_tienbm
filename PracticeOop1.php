<?php

class ExerciseString 
{
    public $Check1;
    public $Check2;

    public function readFile($file)
    {
        $myFile = fopen($file,"r");
        if (!$myFile)
            return "Open $file failed<br>";
        else {
            return fread($myFile, filesize($file));
        }
        // fclose($myFile);
    }

    public function checkValidString($chuoi) 
    {
        $value1 = "book";
        $value2 = "restaurant";
        
        $case1 = substr_count($chuoi, $value1);
        $case2 = substr_count($chuoi, $value2);
       
        return $case1 >= 1 ? $case2 === 0 : $case2 >= 1;
    }

    public function writeFile($array)
    {
        $myFile = fopen('result_file.txt',"w");
        fwrite($myFile,$array);
        fclose($myFile);
    }

}

$object1 = new ExerciseString();
$object1->Check1 = $object1->checkValidString($object1->readFile('file1.txt'));
$object1->Check2 = $object1->checkValidString($object1->readFile('file2.txt'));

if ($object1->Check1) {
    $array1 =  "check1 là chuỗi hợp lệ.\n";
}   else {
    $array1 = "check1 là chuỗi không hợp lệ.\n";
}

if ($object1->Check2) {
    $n = substr_count($object1->readFile('file2.txt'), ".");
    $array2 ="check2 là chuỗi hợp lệ. Chuỗi bao gồm $n câu.";
}   else {
    $n = substr_count($object1->readFile('file2.txt'), ".");
    $array2 = "check2 là chuỗi không hợp lệ.Chuỗi bao gồm $n câu.";
}

$array = $array1 . $array2;
$object1->writeFile($array);

?>